select * from bond 
where cusip = '28717RH95'

select * from bond
order by maturity ASC

select sum(quantity * price) from bond 

select cusip, ((coupon/100) * quantity) from bond

select * from bond b
join rating r on r.ordinal <= 3

select avg(price) as avgprice, avg(coupon) as avgcoupon from bond b 
group by rating

select b.cusip, b.rating, (b.coupon/b.price), r.expected_yield from bond b
join rating r on b.rating = r.rating
where r.expected_yield >= (b.coupon/b.price)
