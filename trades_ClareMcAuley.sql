select * from trader tr
join position p on p.trader_ID = tr.ID
join trade ts on p.opening_trade_ID = ts.ID
join trade tss on p.closing_trade_ID = tss.ID
where tr.ID = 1 and ts.stock = 'MRK'

select sum(ts.size * ts.price * 
(case WHEN ts.buy = 0 THEN 1 
WHEN ts.buy = 1 THEN -1 END)) 
from trade ts
join position p on p.opening_trade_ID = ts.ID 
or p.closing_trade_ID = ts.ID
where p.trader_ID = 1

create view pl_traders as
select sum(ts.size * ts.price * 
(case WHEN ts.buy = 0 THEN 1 
WHEN ts.buy = 1 THEN -1 END)) as p_or_l
from trade ts
join position p on p.opening_trade_ID = ts.ID 
or p.closing_trade_ID = ts.ID

select * from pl_traders